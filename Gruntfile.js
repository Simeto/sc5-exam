

module.exports = function(grunt) {
    var gulp = require('gulp'),
        styleguide = require('sc5-styleguide');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        gulp: {
            'styleguide-generate': function () {
                /*put styleguide css, js and html into this folder*/
                var outputPath = 'outputDir';
                /*Here takes kss*/
                return gulp.src(['css/*.scss'])
                    .pipe(styleguide.generate({
                        title: 'SPS Test Styleguide',
                       server: true,
                        rootPath: outputPath
                    }))
                    .pipe(gulp.dest(outputPath));
            },
            'styleguide-applystyles': function () {
                    //get css files from your css style directory
                /*Here takes styles*/
                gulp.src('css/*.css')
                    .pipe(styleguide.applyStyles())
                    //Creates a directory into which puts css files and related styleguide css
                    .pipe(gulp.dest('outputDir'));
            }
        },
        watch: {
           scss: {
               /*get scss variables*/
                files: 'css/*.scss',
                tasks: ['scss', 'gulp:styleguide-generate', 'gulp:styleguide-applystyles']
            }
        }
    });
    grunt.loadNpmTasks('grunt-gulp');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['gulp:styleguide-generate', 'gulp:styleguide-applystyles', 'watch']);
}